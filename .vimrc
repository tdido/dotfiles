runtime! debian.vim

set nocompatible               " be iMproved
filetype off                   " required!
filetype plugin indent on

syntax on
set clipboard=unnamedplus
set nu
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hidden             " Hide buffers when they are abandoned

set expandtab
set tabstop=8
set softtabstop=4
set shiftwidth=4
set autoindent
set hlsearch
set background=dark
set tw=80

set encoding=utf-8
set fileencodings=utf-8

au BufNewFile,BufRead Snakefile set syntax=snakemake
au BufNewFile,BufRead *.rules set syntax=snakemake
au BufNewFile,BufRead *.snakefile set syntax=snakemake
au BufNewFile,BufRead *.snake set syntax=snakemake
au BufNewFile,BufRead *.smk set syntax=snakemake

nmap =j :%!python -m json.tool<CR>
